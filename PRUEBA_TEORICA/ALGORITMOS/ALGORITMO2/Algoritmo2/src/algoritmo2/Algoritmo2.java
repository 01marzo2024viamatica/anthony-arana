package algoritmo2;

import java.util.Scanner;

public class Algoritmo2 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese el nombre de la cadena: ");
        String cadena = scanner.next();
        //Determinar si la cadena es par o impar
        if (cadena.length() % 2 != 0) {
            System.out.println("La cadena ingresada tiene una longitud impar: " + cadena.length());
        } else {
            int longitud = cadena.length();
            String parte1 = cadena.substring(0, longitud / 2);
            String parte2 = cadena.substring(longitud / 2);
            String resultado = parte2 + parte1;
            System.out.println("Cadena: " + cadena + " (longitud " + longitud + ")");
            System.out.println("Resultado: " + resultado);
        }

    }

}

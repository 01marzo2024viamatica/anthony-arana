package algoritmo1;

import java.util.Random;
import java.util.Scanner;

public class Algoritmo1 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Ingrese el nombre de la provincia (nombre completo): ");
        String provincia = scanner.next().substring(0, 1).toUpperCase(); // Tomar solo la primera letra y convertirla a mayúscula

        char tipo = obtenerTipoVehiculo(scanner); //Obtener letra segun tipo de vehiculo

        System.out.print("Ingrese el número de grupos a generar: ");
        int n = scanner.nextInt(); // Número de grupos a generar

        for (int i = 0; i < n; i++) {
            String placaGrupo = generarGrupoPlacas(provincia, tipo);
            System.out.println("Grupo " + (i + 1) + ": " + placaGrupo);
        }
    }

    public static char obtenerTipoVehiculo(Scanner scanner) {
        System.out.print("Ingrese el tipo de vehículo ");

        //Obtiene el nombre del vehiculo y segun eso permite asginarle la letra
        String tipoVehiculo = scanner.nextLine().toLowerCase();
        while (tipoVehiculo.isEmpty()) {
            tipoVehiculo = scanner.nextLine().toLowerCase();
        }
        if (tipoVehiculo.contains("vehiculos comercial")) {
            return 'C';
        } else if (tipoVehiculo.contains("vehiculos gubernamentales")) {
            return 'E';
        } else if (tipoVehiculo.contains("vehiculos de uso oficial")) {
            return 'X';
        } else if (tipoVehiculo.contains("vehiculos particular")) {
            return 'P';
        } else {
            System.out.println("Tipo de vehículo no válido.");
            return obtenerTipoVehiculo(scanner);
        }
    }

    public static String generarGrupoPlacas(String provincia, char tipo) {
        Random random = new Random();
        StringBuilder placaGrupo = new StringBuilder();
        for (int i = 0; i < 3; i++) {
            char tipoVehiculo = tipo;
            //Si el tipo de vehiculo retornar 'P', es particular y generar una letra random menos la de los tipos anteriores
            if (tipo == 'P') {
                tipoVehiculo = generarLetraAleatoria();
            }
            //Si el tipo de vehiculo retornar 'C', es comercial y generar una letra random entre A, U, Z
            if (tipo == 'C') {
                tipoVehiculo = generarLetraAleatoria("A", "U", "Z");
            }
            String placa = provincia + tipoVehiculo + (random.nextInt(7) + 3) + "-" + generarNumeroAleatorio(1000, 9999);
            placaGrupo.append(placa).append(", ");
        }

        return placaGrupo.toString().trim();
    }

    public static int generarNumeroAleatorio(int min, int max) {
        Random random = new Random();
        return random.nextInt(max - min + 1) + min;
    }

    public static char generarLetraAleatoria(String... letras) {
        Random random = new Random();
        int index = random.nextInt(letras.length);
        return letras[index].charAt(0);
    }

    //Generar una letra aleatoria que no sea las de los otros grupos de vehiculos
    public static char generarLetraAleatoria() {
        Random random = new Random();
        char letra;
        do {
            letra = (char) (random.nextInt(26) + 'A');
        } while (letra == 'A' || letra == 'U' || letra == 'Z' || letra == 'E' || letra == 'X');
        return letra;
    }
}

package com.amag.service;


import java.util.Optional;

import com.amag.model.Usuario;

public interface IUsuarioService {
	
	public Usuario createUser(Usuario usuario);
	
	public Optional<Usuario> getByUsername(String username);
	
	public Usuario updateRol(Long id, Usuario usuarioDetalles);

}

package com.amag.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amag.model.Usuario;
import com.amag.repository.IUsuarioRepository;

import jakarta.persistence.EntityNotFoundException;

@Service
public class UsuarioServiceImpl implements IUsuarioService {

	@Autowired
	private IUsuarioRepository usuarioRepository;

	@Override
	public Usuario createUser(Usuario usuario) {
		return usuarioRepository.save(usuario);

	}

	@Override
	public Optional<Usuario> getByUsername(String username) {
		return usuarioRepository.findByUsername(username);

	}

	@Override
	public Usuario updateRol(Long id, Usuario usuarioDetalles) {
		Usuario usuario = usuarioRepository.findById(id).orElse(null);
		if (usuario != null) {
			usuario.setEmail(usuarioDetalles.getEmail());
			return usuarioRepository.save(usuario);
		} else {
			throw new EntityNotFoundException("El usuario con ID " + id + " no existe");
		}
	}

}

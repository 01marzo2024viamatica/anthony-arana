package com.amag.service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.amag.jwt.JwtService;
import com.amag.model.Rol;
import com.amag.model.Usuario;
import com.amag.repository.IUsuarioRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class AutenticacionServiceImpl implements IAutenticacionService {

	@Autowired
	private IUsuarioRepository usuarioRepository;

	@Autowired
	private IUsuarioService usuarioService;

	@Autowired
	private JwtService jwtService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Override
	public String login(Map<String, String> loginData) {
		String userName = loginData.get("username");
		String password = loginData.get("password");
		authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(userName, password));
		UserDetails user = usuarioRepository.findByUsername(userName).orElseThrow();
		String token = jwtService.getToken(user);
		return token;

	}

	@Override
	public String register(Map<String, Object> registerData) {
		Usuario usuario = new Usuario();
		usuario.setUsername((String) registerData.get("username"));
	    usuario.setEmail((String) registerData.get("email"));
	    usuario.setPassword(passwordEncoder.encode((String) registerData.get("password")));
	    
	    List<Integer> rolesIds = (List<Integer>) registerData.get("roles");

		Set<Rol> userRoles = rolesIds.stream().map(rolId -> {
			Rol rol = new Rol();
			rol.setIdRol(Long.valueOf(rolId));
			return rol;
		}).collect(Collectors.toSet());

		usuario.setRoles(userRoles);
		usuarioService.createUser(usuario);
		return jwtService.getToken(usuario);
	}

}

package com.amag.service;

import java.util.Map;

import org.springframework.http.ResponseEntity;

public interface IAutenticacionService {

	public String login (Map<String, String> loginData);
	
	public String register(Map<String, Object> registerData);
	
}

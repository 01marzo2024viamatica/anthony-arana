package com.amag.service;

import java.util.List;

import com.amag.model.Producto;

public interface IProductoService {
	
	public List<Producto> getAllProducto();

	public Producto getProductoById(Long id);

	public Producto createProducto(Producto producto);
	
	public void deleteProducto(Long id);
}

package com.amag.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.amag.model.Producto;
import com.amag.repository.IProductoRepository;
import com.amag.service.EmailService;
import com.amag.service.IProductoService;

@RestController
@RequestMapping("api/producto")
public class ProductoController {

	@Autowired
	private IProductoService productoService;

	@Autowired
	private IProductoRepository productoRepository;
	
	@Autowired
    private EmailService emailService;
	
	@GetMapping
	public ResponseEntity<?> getAll() {
		return ResponseEntity.ok(productoService.getAllProducto());
	}

	@GetMapping("/{id}")
	public ResponseEntity<?> getById(@PathVariable Long id) {
		return ResponseEntity.ok(productoService.getProductoById(id));
	}

	@PostMapping("/crear")
	public ResponseEntity<?> crearProducto(@RequestBody Producto producto) {
		return ResponseEntity.status(HttpStatus.CREATED).body(productoService.createProducto(producto));
	}

	@PostMapping("/{id}/desactivar")
	public ResponseEntity<?> desactivarProducto(@PathVariable Long id) {
		Producto producto = productoService.getProductoById(id);
		producto.setEstado("Inactivo");
		productoRepository.save(producto);
		return ResponseEntity.ok().build();
	}
	

	@PostMapping("/{id}/activar")
	public ResponseEntity<?> activarProducto(@PathVariable Long id) {
		Producto producto = productoService.getProductoById(id);
		producto.setEstado("Activo");
		productoRepository.save(producto);
		return ResponseEntity.ok().build();
	}

	@PostMapping("/{id}/imagen")
	public ResponseEntity<?> subirImagen(@PathVariable Long id, @RequestParam("file") MultipartFile imagen) {
		try {
			Producto producto = productoService.getProductoById(id);
			if (producto != null) {
				// Guardar la imagen en una carpeta específica o en una base de datos, y
				// actualizar la URL en la entidad
				String carpetaImagenes = "./uploads/";
				String nombreImagen = UUID.randomUUID().toString() + "_" + imagen.getOriginalFilename();
				Files.copy(imagen.getInputStream(), Paths.get(carpetaImagenes + nombreImagen),
						StandardCopyOption.REPLACE_EXISTING);
				producto.setImagen(nombreImagen);
				productoRepository.save(producto);
				return ResponseEntity.ok().build();
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (IOException e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}

	@GetMapping("/{id}/imagen")
	public ResponseEntity<byte[]> obtenerImagen(@PathVariable Long id) {
		Producto producto = productoService.getProductoById(id);
		if (producto != null) {
			producto.getNombre();
			String carpetaImagenes = "./uploads/";
			String nombreImagen = producto.getImagen();
			try {

				Path path = Paths.get(carpetaImagenes + nombreImagen);
				byte[] imagenBytes = Files.readAllBytes(path);
				return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(imagenBytes);
			} catch (IOException e) {
				// Manejo de errores al leer la imagen
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
			}
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@PostMapping("/enviar-correo")
	public ResponseEntity<?> enviarCorreo(@RequestBody Map<String, String> correoData) {
		String email = correoData.get("email");
		String mensaje = correoData.get("mensaje");
		emailService.sendEmail(email, "ENVIAR EMAILS", mensaje);
		return ResponseEntity.ok().build();
	}
}

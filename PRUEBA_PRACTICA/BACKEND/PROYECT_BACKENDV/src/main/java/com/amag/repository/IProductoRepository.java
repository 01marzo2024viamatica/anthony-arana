package com.amag.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.amag.model.Producto;

public interface IProductoRepository  extends JpaRepository<Producto, Long>{

}

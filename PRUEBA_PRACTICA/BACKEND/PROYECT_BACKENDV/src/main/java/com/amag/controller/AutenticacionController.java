package com.amag.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.amag.service.IAutenticacionService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("api/auth")
public class AutenticacionController {

	@Autowired
	private IAutenticacionService autenticacionService;

	@PostMapping(value = "login")
	public ResponseEntity<?> login(@RequestBody Map<String, String> loginData) {
		Map<String, Object> response = new HashMap<>();
        response.put("token", autenticacionService.login(loginData));        
        return ResponseEntity.ok(response);
	}

	
	@PostMapping(value = "register")
	public ResponseEntity<?> register(@Valid @RequestBody Map<String, Object> loginData, BindingResult result) {
		Map<String, Object> response = new HashMap<>();
        response.put("token", autenticacionService.register(loginData));        
        return ResponseEntity.ok(response);
	}
}

package com.amag.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amag.model.Producto;
import com.amag.repository.IProductoRepository;

@Service
public class ProductoServiceImpl implements IProductoService {
	@Autowired
	private IProductoRepository productoRepository;

	@Override
	public List<Producto> getAllProducto() {
		return (List<Producto>) productoRepository.findAll();
	}

	@Override
	public Producto getProductoById(Long id) {
		return productoRepository.findById(id).orElse(null);

	}

	@Override
	public Producto createProducto(Producto producto) {
		return productoRepository.save(producto);
	}

	@Override
	public void deleteProducto(Long id) {
		productoRepository.deleteById(id);		
	}

}

import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/login/service/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
  user = localStorage.getItem('user');
  nombres: string = "";
  constructor(private apiLogin: AuthService, private router: Router) { }
  ngOnInit(): void {
    if (this.user) {
      const user = JSON.parse(this.user);
      this.nombres = user.usuario ;
      
    }
  }

  logout() {
      localStorage.removeItem('session');
      localStorage.removeItem('token');
      localStorage.removeItem('user');

      this.router.navigate(['/login']);

  }
}

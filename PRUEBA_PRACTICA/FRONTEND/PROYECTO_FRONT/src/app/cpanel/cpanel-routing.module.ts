import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CpanelComponent } from './cpanel.component';
import { HomeComponent } from './home/home.component';
import { ProductosComponent } from './productos/productos.component';


const routes: Routes = [
  { 
    path: 'principal', component: CpanelComponent,
    children: [
      { path: '', component: HomeComponent },
      { path: 'productos', component: ProductosComponent }
    ]
  },
  
];
@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes, { useHash: true })
  ], 
  exports: [
    RouterModule
  ]
})
export class CpanelRoutingModule { }

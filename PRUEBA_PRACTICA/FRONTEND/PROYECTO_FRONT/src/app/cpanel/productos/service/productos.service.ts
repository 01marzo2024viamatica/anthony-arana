import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductosService {

  private token = localStorage.getItem("token"); // Reemplaza 'TuTokenBearer' por tu token real
  constructor(private http: HttpClient) { }

  registrarProductos(datos: any) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${this.token}`
    });
    return this.http.post<any>(`/api/producto/crear`, datos, { headers: headers })
  }

   //Obtiene los usuarios de la BD
   obtenerProductos() {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${this.token}`
    });
    return this.http.get<any>(`/api/producto`, { headers: headers })
  }

  
  desactivar(id: number) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${this.token}`
    });
    return this.http.post<any>(`/api/producto/${id}/desactivar`,null,{ headers: headers });
  }

  activar(id: number) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${this.token}`
    });
    return this.http.post<any>(`/api/producto/${id}/activar`,null,{ headers: headers });
  }


  subirImagen(id: number, imagen: File) {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.token}`
    });
    const formData = new FormData();
    formData.append("file", imagen, imagen.name);
    return this.http.post<any>(`/api/producto/${id}/imagen`, formData, { headers })
  }

  mostrarImagen(id: number): Observable<Blob>{
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.token}`
    });
    return this.http.get(`/api/producto/${id}/imagen`, { headers, responseType: 'blob' });
  }

  enviarCorreo(datos: any){
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${this.token}`
    });
    return this.http.post<any>(`/api/producto/enviar-correo`, datos, { headers: headers })
  }
}

import { Component, TemplateRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalService } from './service/modal.service';
import { ProductosService } from './service/productos.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css']
})
export class ProductosComponent {
  ProductoList: any;
  seleccionarImagen: any = null;
  imagenUrl: any = null;
  idProducto: number = 0;
  //Variables para paginacion
  paginaActual = 1;
  registrosPorPagina = 5;
  //Variables para filtro
  ProductoListFiltrada: any;
  nombre = "";
  fechaIngreso = "";
  fechaVenta = "";
  valor = "";
  estado = "0";

  constructor(private productoService: ProductosService, private datePipe: DatePipe, private modalService: ModalService, private toastr: ToastrService) { }

  crearProductoForm = new FormGroup({
    nombre: new FormControl('', Validators.required),
    descripcion: new FormControl('', Validators.required),
    valor: new FormControl('', [Validators.required, Validators.pattern(/^\d+(\.\d+)?$/), Validators.min(1)]),
    marca: new FormControl('', Validators.required),
    fechaVenta: new FormControl('', Validators.required),

  });

  detallesProductoForm = new FormGroup({
    nombre: new FormControl({ value: '', disabled: true }),
    descripcion: new FormControl({ value: '', disabled: true }),
    valor: new FormControl({ value: '', disabled: true }),
    marca: new FormControl({ value: '', disabled: true }),
    fechaVenta: new FormControl({ value: '', disabled: true }),
    fechaCreacion: new FormControl({ value: '', disabled: true }),
  });

  emailForm = new FormGroup({
    correo: new FormControl('', [Validators.required, Validators.email]),
    mensaje: new FormControl('', Validators.required ),
  });

  ngOnInit(): void {
    this.obtenerProductos();
  }

  //Obtiene el registro de productos de la BD
  obtenerProductos() {
    this.productoService.obtenerProductos().subscribe(response => {
      this.ProductoList = response.map((producto: any) => {
        producto.fechaCreacion = this.datePipe.transform(producto.fechaCreacion, 'dd/MM/yyyy hh:mm:ss');
        producto.fechaVenta = this.datePipe.transform(producto.fechaVenta, 'dd/MM/yyyy');
        return producto;
      });
      this.ProductoListFiltrada = this.ProductoList
    }, error => {
      this.toastr.error(error, 'Error');
    });
  }

  //Permite registrar los datos del producto
  registrarProductos() {
    let nombre = this.crearProductoForm.get('nombre')!.value ?? ''
    let descripcion = this.crearProductoForm.get('descripcion')!.value ?? ''
    let valor = this.crearProductoForm.get('valor')!.value ?? ''
    let marca = this.crearProductoForm.get('marca')!.value ?? ''
    let fechaVenta = this.crearProductoForm.get('fechaVenta')!.value ?? ''
    let producto = {
      nombre: nombre,
      descripcion: descripcion,
      valor: valor,
      marca: marca,
      fechaVenta: fechaVenta,
    };

    if (this.crearProductoForm.valid) {
      this.productoService.registrarProductos(producto).subscribe(response => {
        this.toastr.success("Se registro con exito", 'Producto');
        this.closeModal();
        this.obtenerProductos();
      }, error => {
        this.toastr.error(error, 'Error');
      });
    } else {
      this.toastr.error("Existen campos vacios", 'Error');

    }

  }

  //Subir la imagen del base datos a un usuario especificos
  subirImagen() {
    this.productoService.subirImagen(this.idProducto, this.seleccionarImagen).subscribe(response => {
      this.toastr.success('Se subio correctamente', 'Imagen');
      this.closeModal();
    }, error => {
      this.toastr.error(error, 'Error');
    });
  }

  //Mostar la imagen del base datos con el usuario especificos
  mostrarImagen(id: number) {
    this.imagenUrl = null;
    this.productoService.mostrarImagen(id)
      .subscribe((imagen: any) => {
        const reader = new FileReader();
        reader.readAsDataURL(imagen);
        reader.onloadend = () => {
          this.imagenUrl = reader.result;
        };
      }, error => {
      });
  }

  //Limpiar los datos del modelo
  limpiarModal() {
    this.crearProductoForm.reset();
  }

  //Desactivar el producto
  desactivarProducto(idRegistro: number) {
    this.productoService.desactivar(idRegistro).subscribe(response => {
      this.toastr.success('Producto inactivo', 'Inactivo');
      this.obtenerProductos();
    }, error => {
      console.log(error)
      this.toastr.error(error, 'Error');
    });
  }

  //Activa el producto
  activarProducto(idRegistro: number) {
    this.productoService.activar(idRegistro).subscribe(response => {
      this.toastr.success('Producto activo', 'Activo');
      this.obtenerProductos();
    }, error => {
      console.log(error)
      this.toastr.error(error, 'Error');
    });
  }

  //Abre el modal de productos con detalles
  openModalProductoDetalles(template: TemplateRef<any>, data: any) {
    this.detallesProductoForm.reset();
    this.modalService.openModal(template);
    this.idProducto = data.idProducto;
    this.mostrarImagen(this.idProducto);
    this.detallesProductoForm.patchValue(
      {
        nombre: data.nombre,
        descripcion: data.descripcion,
        valor: data.valor,
        marca: data.marca,
        fechaVenta: data.fechaVenta,
        fechaCreacion: data.fechaCreacion,
      }
    );
  }

  //Carga la imagen
  onFileSelected(event: any) {
    this.seleccionarImagen = event.target.files[0];
  }
  //Abre el modal para registar productos
  openModalRegistrar(template: TemplateRef<any>) {
    this.modalService.openModal(template);

  }

  //Abre el modal para cargar la imagen del producto
  openModalSubirImagen(template: TemplateRef<any>, id: number) {
    this.modalService.openModal(template);
    this.idProducto = id;
    console.log(this.idProducto)

  }
  //Abre el modal para cargar la imagen del producto
  openModalEmail(template: TemplateRef<any>) {
    this.modalService.openModal(template);
  }
  closeModal() {
    this.modalService.closeModal();
  }

  //Filtrar productos 
  public filtrar() {
    this.ProductoListFiltrada = this.ProductoList.filter((producto: any) => {

      let filtroNombre = true;
      if (this.nombre) {
        filtroNombre = producto.nombre.toLowerCase().includes(this.nombre.toLowerCase());
      }

      let filtroFechaIngreso = true;
      if (this.fechaIngreso) {
        let fecha = this.datePipe.transform(this.fechaIngreso, 'dd/MM/yyyy');
        filtroFechaIngreso = producto.fechaCreacion.includes(fecha);
      }

      let filtroFechaVenta = true;
      if (this.fechaVenta) {
        let fecha = this.datePipe.transform(this.fechaVenta, 'dd/MM/yyyy');
        filtroFechaVenta = producto.fechaVenta.includes(fecha);
      }

      let filtroValor = true;
      if (this.valor) {
        filtroValor = producto.valor.toString().includes(this.valor);
      }

      let filtroEstado = true;
      if (this.estado != "0") {
        filtroEstado = producto.estado === this.estado;
      }

      return filtroNombre && filtroFechaIngreso && filtroFechaVenta && filtroValor && filtroEstado;
    });
  }

  //Enviar correo
  public enviarCorreo(){
    let correo = this.emailForm.get('correo')!.value ?? ''
    let mensaje = this.emailForm.get('mensaje')!.value ?? ''
   
    let email = {
      email: correo,
      mensaje: mensaje
    };

    if (this.emailForm.valid) {
      this.productoService.enviarCorreo(email).subscribe(response => {
        this.toastr.success("Se envio con exito", 'Correo');
        this.closeModal();
        this.emailForm.reset();
      }, error => {
        console.log()
        this.toastr.error(error, 'Error');
      });
    } else {
      this.toastr.error("Verifique los campos", 'Error');

    }
  }
}

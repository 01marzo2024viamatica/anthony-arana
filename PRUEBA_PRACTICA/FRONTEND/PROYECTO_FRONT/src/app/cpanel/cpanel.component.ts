import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cpanel',
  templateUrl: './cpanel.component.html',
  styleUrls: ['./cpanel.component.css']
})
export class CpanelComponent {

  constructor(private router: Router) { }

  ngOnInit(): void {
    if (localStorage.getItem('session') != 'active' || localStorage.getItem('session') ===null) {
      this.router.navigate(['/login']);
    }
    console.log()
  }
}
